import sys

def es_primo(numero):

    for i in range(2,numero):
        if (numero%i)==0:
            # es divisible
            return False

    return True

def divisores(n):
  divisor = 0

  print("Divisores:")
  sumatorio=0
  lista=[]
  for i in range(1,n):
    if n%i==0:
        sumatorio=sumatorio+i
        lista.append(i)

  if sumatorio==n:
 	print(n,'es un numero perfecto y sus divisores propios son los numeros que estan en la siguiente lista: ',lista,'\n')
  elif sumatorio < n:
    print(n,'es un numero deficiente y sus divisores propios son los numeros que estan en la siguiente lista: ',lista,'\n')
  else:
 	print(n,'es un numero abundante y sus divisores propios son los numeros que estan en la siguiente lista: ',lista,'\n')

if __name__ == "__main__":
    if sys.argv < 2:
        print('Numero invaldo de arguentos, se espera una lista de numeros')
    else:
        i = 1
        while i < len(sys.argv):
            num = sys.argv[i]
            print('numero ', num)
            if num==0:
                break
            if es_primo(int(num)):
                print(num,'es un numero primo y por definicion los numeros primos son deficientes\n')
            else:
                divisores(int(num))
            i += 1
