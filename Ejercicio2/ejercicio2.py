import json
import re, os
from collections import OrderedDict
import datetime
import pandas as pd
import glob
import ast


filename1 = "data1.json"
filename2 = "data2.json"
filename3 = "data3.json"

fileout1 = "data1_new.json"
fileout2 = "data2_new.json"
fileout3 = "data3_new.json"

fd1 = "dataf1.json"
fd2 = "dataf2.json"
fd3 = "dataf3.json"

def jsonDefault(object):
    return object.__dict__

#def read_jsonf(filename):
with open(filename1, 'r') as f1:
    json_str = json.loads(f1.read(), object_pairs_hook=OrderedDict)

f1.close()
print(json_str)
#    return json_str

data = {}
with open(fileout1, 'w') as fo1:
    for row in json_str:
        data['date'] = datetime.datetime.fromtimestamp(row['d']/1000).strftime('%Y-%m-%d')
        data['categ'] = row['cat'].upper()
        data['value'] = row['value']
        print(data['date'], data['categ'], data['value'])
        json.dump(data, fo1)

fo1.close()

open_file = open(fileout1,'r')
read_file = open_file.read()
regex = re.compile(r'}{')
read_file = regex.sub('},{', read_file)
write_file = open(fd1,'w')
write_file.write(read_file)
open_file.close()
write_file.close()

with open(filename2, 'r') as f2:
    json_str2 = json.loads(f2.read(), object_pairs_hook=OrderedDict)

f2.close()

data = {}
with open(fileout2, 'w') as fo2:
    for row in json_str2:
        data['date'] = row['myDate']
        data['categ'] = row['categ']
        data['value'] = row['val']
        #data.append({'date':date, 'categ':categ, 'value':value})
        print(data['date'], data['categ'], data['value'])
        json.dump(data, fo2)

fo2.close()

open_file = open(fileout2,'r')
read_file = open_file.read()
regex = re.compile(r'}{')
read_file = regex.sub('},{', read_file)
write_file = open(fd2,'w')
write_file.write(read_file)
open_file.close()
write_file.close()

with open(filename3, 'r') as f3:
    json_str3 = json.loads(f3.read(), object_pairs_hook=OrderedDict)

f3.close()

pattern1 = "\d{4}-\d{2}-\d{2}"
pattern2 = "#.*#"
pattern3 = re.compile(r'[\W]')
#[Cc][Aa][Tt]\s\d{1}"

data = {}
with open(fileout3, 'w') as fo3:
    for row in json_str3:
        data['date'] = re.findall(pattern1, row['raw'])
        data['categ'] = re.findall(pattern2, row['raw'])
        data['value'] = row['val']
        #data.append({'date':date, 'categ':categ, 'va['date']+','+data['categ']+','+data['value']lue':value})
        print(data['date'], data['categ'], data['value'])
        json.dump(data, fo3)


fo3.close()

open_file = open(fileout3,'r')
read_file = open_file.read()
regex = re.compile(r'[#\[\]]')
read_file = regex.sub('', read_file)
regex = re.compile(r'}{')
read_file = regex.sub('},{', read_file)
write_file = open(fd3,'w')
write_file.write(read_file,)
open_file.close()
write_file.close()

glob_data = []
for filef in glob.glob('datao*.json'):
    open_file = open(filef,'r')
    #data = open_file.read().split(',')
    glob_data.append(json.loads(open_file.read()))

print(glob_data)

with open('finalFile.json', 'w') as f:
    json.dump(glob_data, f)
