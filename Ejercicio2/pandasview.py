import pandas as pd
import json
import matplotlib.pyplot as plt

df_json = pd.read_json('datafinal.json', lines=True)
print(df_json)

dataframe = pd.DataFrame(df_json)
df1 = dataframe.groupby(['categ', 'date'])['value'].agg(['sum'])

print(df1)
df1.plot()

df2 = dataframe[['categ', 'value']]
df2 = df2.groupby(['categ']).sum()
print(df2)
df2.plot.pie(subplots=True, figsize=(6, 6))
