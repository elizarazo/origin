var fs = require('fs');

function stringToJson(input) {
  var result = [];

  //replace leading and trailing [], if present
  input = input.replace(/^\[/,'');
  input = input.replace(/\]$/,'');

  //change the delimiter to
  input = input.replace(/},{/g,'};;;{');

  // preserve newlines, etc - use valid JSON
  //https://stackoverflow.com/questions/14432165/uncaught-syntaxerror-unexpected-token-with-json-parse
  input = input.replace(/\\n/g, "\\n")
  .replace(/\\'/g, "\\'")
  .replace(/\\"/g, '\\"')
  .replace(/\\&/g, "\\&")
  .replace(/\\r/g, "\\r")
  .replace(/\\t/g, "\\t")
  .replace(/\\b/g, "\\b")
  .replace(/\\f/g, "\\f");
  // remove non-printable and other non-valid JSON chars
  input = input.replace(/[\u0000-\u0019]+/g,"");

  input = input.split(';;;');

  input.forEach(function(element) {
    // console.log(JSON.stringify(element));

    result.push(JSON.parse(JSON.stringify(element)));
  }, this);

  return result;
}

var data1 = fs.readFileSync('./dataf1.json', 'utf-8');
var dataf1 =  stringToJson(data1);

var data2 = fs.readFileSync('./dataf2.json', 'utf-8');
var dataf2 =  stringToJson(data2);

dataf1 = dataf1.concat(dataf2.sort());

var data3 = fs.readFileSync('./dataf3.json', 'utf-8');
var dataf3 =  stringToJson(data3);

dataf1 = dataf1.concat(dataf3.sort());

fs.writeFile('datafinal.json', dataf1.sort(), function (err) {
  if (err) throw err;
  console.log('Saved!');
});
